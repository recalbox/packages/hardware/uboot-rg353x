# Instructions

For building Anbernic RG353x uboot, first clone the rk356x-uboot repository and build the uboot.img, example:

```
wget https://releases.linaro.org/components/toolchain/binaries/7.4-2019.02/aarch64-linux-gnu/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu.tar.xz -O - | tar -xJvf - -C /tmp
git clone https://gitlab.com/recalbox/packages/hardware/rk356x-uboot.git
cd rk356x-uboot
./make.sh CROSS_COMPILE=/tmp/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu- rk3566-anbernic-rg353x --spl-new
```
This makes the `uboot.img`.

`idbloader.img` is generated from rockchip/rkbin and mkimage tool from uboot. Still in `rk356x-uboot` dir:
```
./tools/mkimage -n rk3568 -T rksd -d ../rkbin/bin/rk35/rk3566_ddr_1056MHz_v1.21.bin:../rkbin/bin/rk35/rk356x_spl_v1.13.bin -C bzip2 /tmp/idbloader.img
```

Flash `idbloader.img` at sector 64 and `uboot.img` at sector 16384:
```
dd if=idbloader.img of=/dev/mmcblkX seek=64 conv=notrunc
dd if=uboot.img of=/dev/mmcblkX seek=16384 conv=notrunc
```
